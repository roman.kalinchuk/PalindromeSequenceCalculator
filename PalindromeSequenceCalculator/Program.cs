﻿using System;
using System.Diagnostics;
using System.Numerics;

namespace PalindromeSequenceCalculator
{
    class Program
    {
        private static readonly INumberReverser Reverser;

        static Program()
        {
            Reverser = new StringNumberReverser();
        }

        static void Main(string[] args)
        {
            var s = Stopwatch.StartNew();
            int operationsCount = 0;
            var maxOperationsCount = operationsCount;
            var hardestToGetSequenceNumber = 0;
            for (int i = 10; i < 9999; i++)
            {
                operationsCount = GetOperationsToMakePalindromeCount(i);
                if (operationsCount > maxOperationsCount)
                {
                    maxOperationsCount = operationsCount;
                    hardestToGetSequenceNumber = i;
                }
            }
            Console.WriteLine($@"Total elapsed time : {s.ElapsedMilliseconds} Hardest to get palindrome number : {hardestToGetSequenceNumber}
sequence length: {maxOperationsCount}");
            Console.ReadLine();
        }

        private static int GetOperationsToMakePalindromeCount(BigInteger numberToCheck)
        {
            var operationsCount = 0;
            var temp = numberToCheck;
            var s = Stopwatch.StartNew();
            while (!CheckIfPalindrome(temp))
            {
                temp = StackWithReversed(temp);
                operationsCount++;
                if (operationsCount > 1000)
                {
                    return -1;
                }
            }
            Console.WriteLine($"Number to check: {numberToCheck} sequence length: {operationsCount} elapsed ticks: {s.ElapsedTicks}");
            return operationsCount;
        }

        private static bool CheckIfPalindrome(BigInteger numberToCheck)
        {
            return numberToCheck.Equals(Reverser.ReverseNumber(numberToCheck));
        }

        private static BigInteger StackWithReversed(BigInteger numberToStack)
        {
            return numberToStack + Reverser.ReverseNumber(numberToStack);
        }
    }
}
