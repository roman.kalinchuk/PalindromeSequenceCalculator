﻿using System.Linq;
using System.Numerics;

namespace PalindromeSequenceCalculator
{
    public interface INumberReverser
    {
        BigInteger ReverseNumber(BigInteger numberToReverse);
    }

    public class StringNumberReverser : INumberReverser
    {
        public BigInteger ReverseNumber(BigInteger numberToReverse)
        {
            return BigInteger.Parse(new string(numberToReverse.ToString().Reverse().ToArray()));
        }
    }

    public class ModuloNumberReverser : INumberReverser
    {
        public BigInteger ReverseNumber(BigInteger numberToReverse)
        {
            BigInteger left = numberToReverse;
            BigInteger rev = 0;
            while (left > 0)
            {
                BigInteger r = left % 10;
                rev = rev * 10 + r;
                left = left / 10;
            }
            return rev;
        }
    }
}